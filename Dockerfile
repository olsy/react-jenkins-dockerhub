FROM node:18-alpine as build

WORKDIR /app

COPY package.json .

COPY package-lock.json .

COPY . .

RUN npm ci

RUN npm run build

FROM nginx:1.23.3-alpine as production

ENV NODE_ENV production

COPY ./nginx/nginx.conf /etc/nginx/nginx.conf

COPY --from=build /app/build /usr/share/nginx/html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
